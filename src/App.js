import React from 'react'
import Routes from './Routes'
import EStyleSheet from 'react-native-extended-stylesheet'

EStyleSheet.build({
  $primaryColor: '#cfe7f1',
  $textColor: '#0c646a',
  $white: '#fff',
  $border: '#e2e2e2',
  $inputText: '#797979',
});

export default () => <Routes />
