import { StackNavigator } from 'react-navigation'
import Home from './screen/Home'
import Random from './screen/Random'
import Input from './screen/Input'

export default StackNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      header: () => null,
    },
  },
  Random: {
    screen: Random,
    navigationOptions: {
      header: () => null,
    },
  },
  Input: {
    screen: Input,
    navigationOptions: {
      header: () => null,
    },
  },
},{
  mode: 'modal',
});
