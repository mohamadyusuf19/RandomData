import React from 'react'
import { PropTypes } from 'prop-types'
//tidak boleh ada kesalahan dalam menulis sintaks, apalagi kalau terbaik
import { View, Image, TextInput, TouchableHighlight } from 'react-native'
import styles from './styles'

const InputWithButton = (props) => {
  const { onPress, buttonText } = props;
  return(
    <View style={styles.container}>
      <TouchableHighlight style={styles.buttonContainer} onPress={onPress}>
        <Image style={styles.buttonText} source={buttonText} />
      </TouchableHighlight>
      <View style={styles.border} />
      <TextInput
        underlineColorAndroid='transparent'
        style={styles.input}
        {...props} />
    </View>
  );
};

InputWithButton.propTypes = {
  onPress: PropTypes.func,
  buttonText: PropTypes.any
};

export default InputWithButton;
