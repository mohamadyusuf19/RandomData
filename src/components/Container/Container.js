import React from 'react';
//propTypes berfungsi untuk mengekspor property diluar Component agar lebih mudah
import { PropTypes } from 'prop-types'
import { View, TouchableWithoutFeedback, Keyboard } from 'react-native'
import styles from './styles'

const Container = ({ children }) => (
  //TouchableWithoutFeedback dan keyboard digunakan untuk menutup keyboard ketika menekan tombol di luar teks input
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    <View style={styles.container}>
      {children}
    </View>
  </TouchableWithoutFeedback>
);

Container.propTypes = {
  children: PropTypes.any,
};

export default Container;
