import React, { Component } from 'react';
import { View, Text, Keyboard, Animated, Platform } from 'react-native';
import styles from './styles'

const ANIMATION_DURATION = 250

class Logo extends Component {
  constructor(props) {
    super(props);

    this.containerImageWidth = new Animated.Value(styles.$largeIcon);
  }
  componentDidMount () {
    let showListener = 'keyboardWillShow'
    let hideListener = 'keyboardWillHide'

    if(Platform.OS === 'android') {
      showListener = 'keyboardDidShow'
      hideListener = 'keyboardDidHide'
    }
    //membuat fungsi ketika keyboard ditampilkan
    // this.keyboardShowListener = Keyboard.addListener('keyboardWillShow', this.function)
    this.keyboardShowListener = Keyboard.addListener(showListener, this._keyboardDidShow);
    this.keyboardHideListener = Keyboard.addListener(hideListener, this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardShowListener.remove();
    this.keyboardHideListener.remove();
  }

    //untuk menjalankan animasi harus memakai arrow function
    //animasi digunakan untuk membuat animasi
    //dimulai dari Animated.timing(this.new Animated.Value(styles), {
    //toValue: hasil ketika fungsi dijalankan,
    //duration: waktu animasi,
    // }).start(); harus ada start untuk menjalankan animasi
  _keyboardDidShow = () => {
    Animated.timing(this.containerImageWidth, {
      toValue: styles.$smallIcon,
      duration: ANIMATION_DURATION,
    }).start();
  }

  _keyboardDidHide = () => {
    Animated.timing(this.containerImageWidth, {
      toValue: styles.$largeIcon,
      duration: ANIMATION_DURATION,
    }).start();
  }

  render() {
    const containerImageStyle = [
      styles.containerImage,
      {width: this.containerImageWidth, height: this.containerImageWidth}
    ];
    return (
      <View style={styles.container}>
        <Animated.Image
          resizeMode="contain"
          style={containerImageStyle}
          source={require('../Images/arrows.png')}
        />
        <Text style={styles.textLogo}>Random Data</Text>
      </View>
    )
  }
}

export default Logo;
