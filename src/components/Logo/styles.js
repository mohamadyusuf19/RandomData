import { Dimensions } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
const imageWidth = Dimensions.get('window').width / 2;

export default EStyleSheet.create({
  $largeIcon: imageWidth,
  $smallIcon: imageWidth / 2,
  container: {
    alignItems: 'center'
  },
  containerImage: {
    alignItems: 'center',
    justifyContent: 'center',
    width: '$largeIcon',
    height: '$largeIcon',
  },
  textLogo: {
    fontSize: 28,
    fontWeight: '900',
    letterSpacing: -0.5,
    marginTop: 15,
    color: '$textColor',
  }
})
