import React from 'react'
import { PropTypes } from 'prop-types'
import styles from './styles'
import {TouchableHighlight, View, Text} from 'react-native'

const ButtonLogin = ({ onPress }) => (
  <View style={styles.container}>
    <TouchableHighlight style={styles.button} onPress={onPress}>
      <Text style={styles.textButton}>LOGIN</Text>
    </TouchableHighlight>
  </View>
)

ButtonLogin.propTypes = {
  onPress: PropTypes.func
};

export default ButtonLogin
