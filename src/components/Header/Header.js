import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native'

class Header extends Component {
  render() {
    return (
      <View style={styles.header}>
        <Text style={styles.textHeader}>Hasil Random</Text>
      </View>
    )
  }
}

export default Header;

const styles = StyleSheet.create({
  header: {
    height: 50,
    backgroundColor: "#0c646a",
    alignItems:"center",
    paddingRight: 5
  },
  textHeader: {
    paddingTop: 10,
    fontSize: 18,
    alignItems: 'center',
    justifyContent: 'center',
    color: '#fff'
  }
})
