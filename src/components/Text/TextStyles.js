import React from 'react'
import {PropTypes} from 'prop-types'
import moment from 'moment'
import styles from './styles'
import { Text } from 'react-native'

const TextStyles = ({ date }) => (
    <Text style={styles.text}>
      Today : {moment(date).format('MMMM D YYYY, h:mm:ss a')}
    </Text>
);

TextStyles.propTypes = {
  date: PropTypes.object,
}

export default TextStyles;
