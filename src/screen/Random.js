import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Button,
  Text,
  FlatList,
  TouchableOpacity,
  Image
} from "react-native";
import data from "../Data.json";
import Header from "../components/Header/Header";

export default class Random extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedName: []
    };
  }

  componentDidMount() {
    this._getRandomColor();
  }

  _getRandomColor() {
    const myShows = [
      "Ridwan",
      "Muiz",
      "Ahmad",
      "Firman",
      "Yoko",
      "Umar",
      "Hamdan",
      "Affan",
      "Yusuf",
      "Mustofa",
      "Afif",
      "Adit",
      "Yahya",
      "Fadil",
      "Arif",
      "Adi",
      "Aziz"
    ];
    for (let i = myShows.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random().toString() * (i + 1));
      [myShows[i], myShows[j]] = [myShows[j], myShows[i]];
      this.setState({
        selectedName: myShows
      });
    }
  }

  render() {
    return (
      <View style={styles.MainContainer}>
        <Header />
        <FlatList
          data={this.state.selectedName}
          keyExtractor={(x, i) => i.toString()}
          style={{ backgroundColor: "#fff" }}
          renderItem={({ item }) => (
            <View style={styles.row}>
              <TouchableOpacity style={styles.iconContainer}>
                <Image
                  style={styles.iconContainer}
                  source={require("../components/Images/account.png")}
                />
              </TouchableOpacity>
              <View style={styles.info}>
                <View style={styles.total}>
                  <Text style={styles.pengguna}>{item}</Text>
                </View>
              </View>
            </View>
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1
  },
  Main: {
    alignItems: "center",
    justifyContent: "center"
  },
  total: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 5,
    paddingLeft: 0
  },
  info: {
    flex: 1,
    paddingLeft: 10,
    paddingTop: 15,
    borderBottomWidth: 1,
    borderColor: "#BDBDBD"
  },
  pengguna: {
    fontWeight: "bold",
    fontSize: 17,
    color: "#000"
  },
  row: {
    flexDirection: "row",
    height: 75
  },
  iconContainer: {
    alignItems: "center",
    borderColor: "#f1f1f1",
    borderRadius: 28,
    borderWidth: 1,
    justifyContent: "center",
    height: 56,
    width: 56,
    margin: 10
  }
});
// [{
//   nama: ['Ridwan', 'Muiz', 'Ahmad', 'Firman', 'Yoko', 'Umar', 'Hamdan', 'Affan',
//   'Yusuf', 'Mustofa', 'Afif', 'Adit', 'Yahya', 'Fadil', 'Arif', 'Adi', 'Aziz'],
//   area: ['A1','A2','A3','A4']}
// ];
