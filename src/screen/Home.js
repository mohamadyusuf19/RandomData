import React, {Component} from 'react';
import { StatusBar } from 'react-native'
import { Container } from '../components/Container'
import { Logo } from '../components/Logo'
import { InputWithButton } from '../components/TextInput'
import { ButtonLogin } from '../components/Button'
import { TextStyles } from '../components/Text'

const TEMP_BASE_CURRENCY = require('../components/Images/account.png');
const TEMP_QUOTE_CURRENCY = require('../components/Images/lock.png');
const TEMP_BASE_PRICE = 'Username';
const TEMP_QUOTE_PRICE = 'Password';
const TEMP_DATE = new Date();

class Home extends Component {
  handlePressBaseCurrency = () => {
    console.log('press base')
  }
  handlePressQuoteCurrency = () => {
    console.log('press quote')
  }
  handleTextChange = (text) => {
    console.log('change text', text);
  }

  navigationInput = () => {
    this.props.navigation.navigate('Input');
  }

  render() {
    return(
      <Container>
        <StatusBar
          translucent={false}
          barStyle="light-content"
          />
          <Logo />
          <InputWithButton
            buttonText={TEMP_BASE_CURRENCY}
            onPress={this.handlePressBaseCurrency}
            placeholder={TEMP_BASE_PRICE}
            onChangeText={this.handleTextChange}
          />
          <InputWithButton
            secureTextEntry={true}
            buttonText={TEMP_QUOTE_CURRENCY}
            onPress={this.handlePressQuoteCurrency}
            placeholder={TEMP_QUOTE_PRICE}
          />
          <TextStyles
            date={TEMP_DATE}
          />
          <ButtonLogin
            onPress={this.navigationInput}
          />
      </Container>
    );
  }
}

export default Home;
//KeyboardAvoidingView digunakan untuk menampilkan komponen ketika keyboard ditanpilkan
// <KeyboardAvoidingView behavior="padding">
// </KeyboardAvoidingView>
